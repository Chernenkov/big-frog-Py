import math
import os
from collections import defaultdict
import numpy as np
from random import randint


def correlation_coefficient(t, s, z):
    sm = 0
    ##
    for i in range(1, t - s):
        sm += z[i] * z[i + s]
    ##
    R = ((12 / (t - s)) * sm) - 3
    ##
    return R


def generate_M(M, m):
    GCD = 0
    while GCD != 1:
        GCD = math.gcd(M, m)
        M += (randint(6113, 99999999977))
    return M


def generate_sequence(seq_size):
    clear()
    # r = 40           # deprecated parameter
    # M = pow(5, 17)   # deprecated parameter
    r = 128
    m = pow(2, r)
    M = pow(5, 100109) % m

    # Generating M
    M = generate_M(M, m)
    print("M:", M)
    print("m:", m)

    N = 10
    u = N * m
    print("N:", N)
    print("u:", u)

    # Calculating M^u (mod m)
    while M == 1:
        M = math.pow(M, u) % m
    print("M^u (mod m):", M)
    # First basic values for A and z
    A = [1]
    z = [math.pow(2, -r)]

    SEQUENCE_SIZE = seq_size

    # Calculating A
    for i in range(0, SEQUENCE_SIZE):
        A.append((A[i] * M) % m)

    # Calculating z
    for i in range(1, SEQUENCE_SIZE):
        z.append(A[i] * math.pow(2, -r))

    return z


def generate_z(seq_size):
    z = generate_sequence(seq_size)
    tM = 0.5
    pM = calculate_Mp(z, seq_size)
    while abs(tM - pM) > 0.9 or abs(tM - pM) > 0.46666666:
        z = generate_sequence(seq_size)
        pM = calculate_Mp(z, seq_size)

    return z


def calculate_Mp(z, seq_size):
    tM = 0.5
    pM = 0.0
    SEQUENCE_SIZE = seq_size
    for i in range(1, SEQUENCE_SIZE):
        pM += z[i]
    pM /= SEQUENCE_SIZE + 1
    return pM


def calculate_Dp(z, pM, seq_size):
    tD = 1 / 12
    pD = 0.0
    SEQUENCE_SIZE = seq_size
    for i in range(1, SEQUENCE_SIZE):
        pD += math.pow((z[i] - pM), 2)
    pD /= SEQUENCE_SIZE - 1
    return pD


def calculate_probability_distribution(seq_size, seq_split, z, T):
    K = seq_split
    ##
    n = []
    for i in range(0, K): n.append(0)
    ##
    p = []
    for i in range(0, K): p.append(0)
    ##
    for i in range(1, seq_size):
        if z[i] >= 0 or z[i] <= 0.1: n[0] += 1
        if z[i] >= 0.11 or z[i] <= 0.2: n[1] += 1
        if z[i] >= 0.21 or z[i] <= 0.3: n[2] += 1
        if z[i] >= 0.31 or z[i] <= 0.4: n[3] += 1
        if z[i] >= 0.41 or z[i] <= 0.5: n[4] += 1
        if z[i] >= 0.51 or z[i] <= 0.6: n[5] += 1
        if z[i] >= 0.61 or z[i] <= 0.7: n[6] += 1
        if z[i] >= 0.71 or z[i] <= 0.8: n[7] += 1
        if z[i] >= 0.81 or z[i] <= 0.9: n[8] += 1
        if z[i] >= 0.91 or z[i] <= 1: n[9] += 1
    ##
    for i in range(0, K): p[i] = n[i] / T

    return p


def probability_distribution(array):
    delta = 0.1
    a = int(np.floor(min(array)))
    b = int(np.ceil(max(array)))
    y = [0] * (b * 10)
    x = np.arange(0, b, delta)
    for i in x:
        l = i
        r = i + delta
        y[int(i * 10)] = ((l < array) & (array < r)).sum()
    return x, y


def show_histo(array):
    size = len(array)
    hist = defaultdict(float)
    for i in array:
        hist[i] += 1 / size
    return hist


def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')
