import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from utils.utils import *
import numpy as np
import core.DiscreteRand as d


def main():
    probabilities = [0.285, 0.152, 0.07, 0.056, 0.288, 0.126, 0.023]
    values = [-82.3, 4.3, 13.1, 28.2, 35.1, 55.3, 92.1]
    rand = d.DiscreteRand(probabilities, values)
    print(rand.math_expect())
    print(rand.dispersion())
    rand_value = []
    SEQUENCE_SIZE = 10000
    z = generate_z(SEQUENCE_SIZE)
    for i in range(0, 500):
            rand_value.append(rand.get_val(z[i]))
    print("Empirical M: ", rand.math_expect_empirical(rand_value))
    print("Empirical D:", rand.dispersion_empirical(rand_value))
    print("M: ", rand.math_expect())
    print("D: ", rand.dispersion())


    hist = show_histo(rand_value)
    a = np.array(values)
    plt.bar(hist.keys(), hist.values(), width=1, label="Bar 1")
    emp = mpatches.Patch(color='blue', label='empirical')
    teor = mpatches.Patch(color='red', label='theoretical')
    plt.legend(handles=[emp, teor])
    plt.bar(a + 1, probabilities, color='r', width=1, label="Bar 1")
    plt.show()


if __name__ == '__main__':
    main()
