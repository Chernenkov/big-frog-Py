import matplotlib.pyplot as plt
import matplotlib.pyplot as plt0
import matplotlib.pyplot as plt1
import matplotlib.pyplot as plt2
import numpy as np
import itertools

from utils.utils import *

# LAB2
SEQUENCE_SIZE = 100000
r = 128
z = generate_z(SEQUENCE_SIZE)
K = 10
T = pow(2, r - 2)

# Exponential distribution
################################################################################
lmbd = 0.5
exp = (-1 / lmbd) * np.log(z)
pM = calculate_Mp(exp, SEQUENCE_SIZE)
pD = calculate_Dp(exp, pM, SEQUENCE_SIZE)
print("EXPONENTIAL DISTRIBUTION: ")
print("labda: ", lmbd)
print("M: ", pM)
print("D: ", pD)
print("Expected M: ", 1 / lmbd)
print("Expected D: ", 1 / (lmbd ** 2))
x, y = probability_distribution(exp)
plt.bar(x, y)
plt.title("Exponential distribution")
plt.show()
#################################################################################

# Uniform distribution
#################################################################################
a = 3
b = 15
unf = [0]
for i in range(1, SEQUENCE_SIZE):
    unf.append(a + (b - a) * z[i])

pM = calculate_Mp(unf, SEQUENCE_SIZE)
pD = calculate_Dp(unf, pM, SEQUENCE_SIZE)
print("UNIFORM DISTRIBUTION: ")
print("a: ", a)
print("b: ", b)
print("M: ", pM)
print("D: ", pD)
print("Expected M: ", a + (b - a) / 2)
print("Expected D: ", ((b - a) ** 2) / 12)
x, y = probability_distribution(unf)
plt.bar(x, y)
plt.title("Uniform distribution")
plt.show()

#################################################################################

# Erlang distribution
#################################################################################
er = np.zeros(SEQUENCE_SIZE, np.float64)
k = 2
for i in range(0, k):
    z = generate_z(SEQUENCE_SIZE)
    arr = (-1 / lmbd) * np.log(z)
    er += arr

pM = calculate_Mp(er, SEQUENCE_SIZE)
pD = calculate_Dp(er, pM, SEQUENCE_SIZE)
print("ERLANG DISTRIBUTION: ")
print("M: ", pM)
print("D: ", pD)
print("Expected M: ", k / lmbd)
print("Expected D: ", k / lmbd ** 2)
x, y = probability_distribution(er)
plt.bar(x, y)
plt.title("Erlang distribution")
plt.show()
#################################################################################

# Normal distribution
#################################################################################
z2 = generate_z(SEQUENCE_SIZE)
normal = [0]
for i in range(1, SEQUENCE_SIZE):
    normal.append(np.sqrt(-2 * np.log(z[i])) * np.cos(2 * np.pi * z2[i]))
x, y = probability_distribution(normal)
# x = itertools.chain(reversed(x), x)
# y = itertools.chain(reversed(y), y)
# y = reversed(y)
pM = calculate_Mp(normal, SEQUENCE_SIZE)
pD = calculate_Dp(normal, pM, SEQUENCE_SIZE)
print("NORMAL DISTRIBUTION: ")
print("M: ", pM)
print("D: ", pD)
print("Expected M: ", 0)
print("Expected D: ", 1)
plt.plot(x, y)
plt.title("Normal distribution")
plt.show()
#################################################################################

# Gauss distribution
#################################################################################
m_exp = 5
d_exp = 2
x = z
gauss = [1]
for i in range(1, SEQUENCE_SIZE):
    gauss.append(normal[i] * d_exp + m_exp)
pM = calculate_Mp(gauss, SEQUENCE_SIZE)
pD = calculate_Dp(gauss, pM, SEQUENCE_SIZE)
print("GAUSS DISTRIBUTION: ")
print("M: ", pM)
print("D: ", pD)
print("Expected M: ", m_exp)
print("Expected D: ", d_exp ** 2)
x, y = probability_distribution(gauss)
plt.plot(x, y)
plt.title("Gauss distribution: ")
plt.show()
##################################################################################
