import matplotlib.pyplot as plt
import matplotlib.pyplot as plt0
import matplotlib.pyplot as plt1
import matplotlib.pyplot as plt2

from utils.utils import *

#############################################
# LAB1
SEQUENCE_SIZE = 100000
r = 128
z = generate_z(SEQUENCE_SIZE)
tM = 0.5
tD = 1 / 12
pM = calculate_Mp(z, SEQUENCE_SIZE)
pD = calculate_Dp(z, pM, SEQUENCE_SIZE)
print("Practical M:", "{0:.5}".format(pM))
print("Theoretic M:", "{0:.5}".format(tM))
print("Practical D:", "{0:.5}".format(pD))
print("Theoretic D:", "{0:.5}".format(tD))
T = pow(2, r - 2)
# Sequence is split to K parts
K = 10
# Histogram for values spread
############################################################################
values = calculate_probability_distribution(SEQUENCE_SIZE, K, z, T)
names = ["0-0.1", "0.11-0.2", "0.21-0.3", "0.31-0.4", "0.41-0.5", "0.51-0.6", "0.61-0.7", "0.71-0.8", "0.81-0.9",
         "0.91-1"]
plt.figure(1, figsize=(9, 4))
high = max(values)
low = min(values)
# plt.gca().set_ylim(low, high)
plt.bar(names, values)
plt.show()
# x, y = probability_distribution(z)
# plt.plot(x, y)
# plt.title("Normal distribution")
# plt.show()
########################################################################
##
# Correlation graphics
# R(T, s = 2, z)
corr = 0
r_values1 = []
r_values2 = []
r_values3 = []
for i in range(5000, 20000):
    r_values1.append(correlation_coefficient(i, 2, z))
    r_values2.append(correlation_coefficient(i, 5, z))
    r_values3.append(correlation_coefficient(i, 10, z))
    if i % 10000 == 0: print("***")
# Drawing correlation coefficient R(T, s, z)
# R(T, s = 2, z)
plt0.plot(r_values1)
plt0.xlabel("R(T, s = 2, z)")
plt0.show()
# R(T, s = 5, z)
plt1.plot(r_values2)
plt1.xlabel("R(T, s = 5, z)")
plt1.show()
# R(T, s = 10, z)
plt2.plot(r_values3)
plt2.xlabel("R(T, s = 10, z)")
plt2.show()
##
# Grouped correlation plots
plt.figure(2, figsize=(15, 6))

ax1 = plt.subplot(311)
plt.plot(r_values1)
ax1.set_ylabel("R(T, s = 2, z)", fontsize=16)

##
ax2 = plt.subplot(312)
plt.plot(r_values2)
ax2.set_ylabel("R(T, s = 5, z)", fontsize=16)

##
ax3 = plt.subplot(313)
plt.plot(r_values3)
ax3.set_ylabel("R(T, s = 10, z)", fontsize=16)
plt.show()
###########################################################
